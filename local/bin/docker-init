#!/bin/sh

set -e

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing required packages"
PKG_LIB="libffi libxml2 libxslt zlib libjpeg-turbo openssl"
PKG_DEV="libffi-dev libxml2-dev libxslt-dev zlib-dev libjpeg-turbo-dev openssl-dev"
docker-install -r -u ${PKG_LIB} ${PKG_DEV}
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing virtualenv"
mkdir -p /app
cd /app
virtualenv -p /usr/bin/python2 virtualenv
cd /app/virtualenv
. bin/activate
pip2 install --upgrade pip
cd /
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing SJVA2 python requirements (core)"
cd /app/virtualenv
wget --no-check-certificate -q https://github.com/soju6jan/SJVA2/raw/ab41037362036b14a770b26f2fcc17563f1a226b/requirements.txt
echo "rsa<4" >> requirements.txt
pip2 install --no-cache-dir -r requirements.txt
if [ -e update_requirements.txt ]; then
	pip2 install --no-cache-dir -r update_requirements.txt
fi
cd /
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing SJVA2 python requirements (extra)"
#uncompyle6
pip2 install --no-cache-dir uncompyle6
#streamlink
ln -sf /usr/include/libxml2/libxml /usr/include/libxml
pip2 install --no-cache-dir streamlink
rm -rf /usr/include/libxml
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Uninstalling unnecessary packages"
docker-install -d -c ${PKG_DEV}
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Moving output files"
mkdir -p /output
mv /app/virtualenv /output/
if [ -z "$(ls -A -- /app)" ]; then
	rm -rf /app
fi
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

